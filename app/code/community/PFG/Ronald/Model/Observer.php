<?php

/**
 * Description of Observer
 *
 * @author ronnie
 */
class PFG_Ronald_Model_Observer {
    /**
     * As the name implies disables products without images
     *
     * @param   Mage_Event_Observer $observer
     * @return  $this
     */
    public function disableProductsWithoutImages($observer)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();
        $product->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
        return $this;
    }
}
